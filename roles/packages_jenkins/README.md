# JENKINS PACKAGE

Install packages for jenkins server.

## Requirements

- [ansible.builtin.apt_key](https://docs.ansible.com/ansible/latest/collections/ansible/builtin/apt_key_module.html)
- [ansible.builtin.apt_repository](https://docs.ansible.com/ansible/latest/collections/ansible/builtin/apt_repository_module.html)
- [ansible.builtin.apt](https://docs.ansible.com/ansible/latest/collections/ansible/builtin/apt_module.html)

## Variables

| name | type | default value | description |
| :--- | :---: | ---: | :--- |
| packages_jenkins_repository_key_url | string | See file | Jenkins repository key url |
| packages_jenkins_repository_key_dest | string | See file | Jenkins repository key destination |
| packages_jenkins_repository | string | See file | Jenkins repository url |
| packages_jenkins_repository_filename | string | See file | Jenkins repository file name |
| packages_jenkins_package_dependencies | List\<string\> | See file | List of packages |
| packages_jenkins_package_jenkins_server | List\<string\> | See file | List of packages |

## Dependencies

This role has dependencies either on Debian or a Debian variant distribution.

## Tags

This role has no tags.

## Example

```yaml
- name: "Setup servers hosts"
  hosts: "servers"
  become: "true"
  roles:
    - packages_jenkins
```

## Authors

[AUTHORS](./AUTHORS)
