# C/C++

Install C and C++ development environment.

## Requirements

- [ansible.builtin.dnf](https://docs.ansible.com/ansible/latest/collections/ansible/builtin/dnf_module.html)

## Variables

| name | type | default value | description |
| :--- | :---: | ---: | :--- |
| ccpp_packages_compilers_rpm | List<string> | See file | List of packages in rpm format |
| ccpp_packages_analysers_rpm | List<string> | See file | List of packages in rpm format |
| ccpp_packages_indexers_rpm | List<string> | See file | List of packages in rpm format |

## Dependencies

This role has dependency either on Fedora or a Fedora variant distribution.

## Tags

This role has no tags.

## Example

```yaml
- name: "Install packages"
  hosts: "workstations"
  become: "true"
  roles:
    - languages_ccpp
```

## Authors

[AUTHORS](./AUTHORS)
