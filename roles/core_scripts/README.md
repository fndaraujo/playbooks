# USER SCRIPTS

Add user scripts in home.

## Requirements

- [ansible.builtin.file](https://docs.ansible.com/ansible/latest/collections/ansible/builtin/file_module.html)
- [ansible.builtin.template](https://docs.ansible.com/ansible/latest/collections/ansible/builtin/template_module.html)

## Variables

| name | type | default value | description |
| :--- | :---: | ---: | :--- |
| core_scripts_path_home_bin | string | "/var/home/fernando/.local/bin" | Location of the scripts |

## Dependencies

This role has no dependencies.

## Tags

This role has no tags.

## Example

```yaml
- name: "Setup workstation hosts"
  hosts: "workstations"
  become: "false"
  roles:
    - core_scripts
```

## Authors

[AUTHORS](./AUTHORS)
