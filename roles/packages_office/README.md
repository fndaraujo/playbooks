# OFFICE PACKAGES

Install packages for office environment.

## Requirements

- [community.general.flatpak](https://docs.ansible.com/ansible/latest/collections/community/general/flatpak_module.html)

## Variables

| name | type | default value | description |
| :--- | :---: | ---: | :--- |
| packages_office_flatpak_remote | string | "https://dl.flathub.org/repo/flathub.flatpakrepo" | Remote repository of Flathub |

## Dependencies

This role has no dependencies.

## Tags

This role has no tags.

## Example

```yaml
- name: "Setup workstation hosts"
  hosts: "workstations"
  become: "false"
  roles:
    - packages_office
```

## Authors

[AUTHORS](./AUTHORS)
