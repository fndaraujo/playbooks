# CONTAINER OCI IMAGES

Pull container images from [docker hub](https://hub.docker.com/).

## Requirements

- [containers.podman.podman_image](https://docs.ansible.com/ansible/latest/collections/containers/podman/index.html)

## Variables

| name | type | default value | description |
| :--- | :---: | ---: | :--- |
| containers_oci_images | List<string> | See file | List of containers images |

## Dependencies

This role has dependency either on Fedora or a Fedora variant distribution.

## Tags

This role has no tags.

## Example

```yaml
- name: "Setup workstation hosts"
  hosts: "workstations"
  become: "false"
  roles:
    - containers_oci
```

## Authors

[AUTHORS](./AUTHORS)
