# BORG systemd service

Add systemd service for borg running inside a toolbx container.

## Requirements

- [ansible.builtin.copy](https://docs.ansible.com/ansible/latest/collections/ansible/builtin/copy_module.html)
- [ansible.builtin.file](https://docs.ansible.com/ansible/latest/collections/ansible/builtin/file_module.html)
- [ansible.builtin.template](https://docs.ansible.com/ansible/latest/collections/ansible/builtin/template_module.html)
- [ansible.builtin.systemd_service](https://docs.ansible.com/ansible/latest/collections/ansible/builtin/systemd_service_module.html)

## Variables

| name | type | default value | description |
| :--- | :---: | ---: | :--- |
| services_toolbx_borg_systemd_user_services_path | string | See file | User systemctl service scripts directory |
| services_toolbx_borg_server_hostname | string | See file | Borg backup server hostname |
| services_toolbx_borg_server_repository_name | string | See file | Name of the backup repository in the Borg server |
| services_toolbx_borg_host_command | string | See file | Command to execute the borg backup tool |
| services_toolbx_borg_host_pattern_list_relative_path | string | See file | Relative path of the pattern list file |

## Dependencies

This role has no dependencies.

## Tags

This role has no tags.

## Example

```yaml
- name: "Install borg toolbx service"
  hosts: "workstations"
  become: "true"
  roles:
    - services_toolbx_borg
```

## Authors

[AUTHORS](./AUTHORS)
