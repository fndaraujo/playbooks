# VISUAL STUDIO CODE TEXT EDITOR

Install Visual Studio Code text editor.

## Requirements

- [ansible.builtin.rpm_key](https://docs.ansible.com/ansible/latest/collections/ansible/builtin/rpm_key_module.html)
- [ansible.builtin.copy](https://docs.ansible.com/ansible/latest/collections/ansible/builtin/copy_module.html)
- [ansible.builtin.dnf](https://docs.ansible.com/ansible/latest/collections/ansible/builtin/dnf_module.html)

## Variables

| name | type | default value | description |
| :--- | :---: | ---: | :--- |
| editors_vscode_packages_rpm | string | See file | Microsoft GPG Key repository |
| editors_vscode_yum_repositories_outers | string | yum.repos.d | Yum third party repositories directory |
| editors_vscode_packages_rpm | List\<string\> | See file | Visual Studio Code rpm packages |

## Dependencies

This role has dependency on either a Fedora or a Fedora variant distribution.

## Tags

This role has no tags.

## Example

```yaml
- name: "Install VS Code text editor"
  hosts: "workstations"
  become: "true"
  roles:
    - editors_vscode
```

## Authors

[AUTHORS](./AUTHORS)
