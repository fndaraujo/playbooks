# JETBRAINS TOOLBOX

Install required packages for jetbrains toolbox.

## Requirements

- [ansible.builtin.dnf](https://docs.ansible.com/ansible/latest/collections/ansible/builtin/dnf_module.html)
- [ansible.builtin.file](https://docs.ansible.com/ansible/latest/collections/ansible/builtin/file_module.html)
- [ansible.builtin.unarchive](https://docs.ansible.com/ansible/latest/collections/ansible/builtin/unarchive_module.html)

## Variables

| name | type | default value | description |
| :--- | :---: | ---: | :--- |
| jetbrains_toolbox_required_packages_rpm | List<string> | See file | List of required packages in rpm format |

## Dependencies

This role has dependency either on Fedora or a Fedora variant distribution.

## Tags

This role has no tags.

## Example

```yaml
- name: "Install packages"
  hosts: "workstations"
  become: "true"
  roles:
    - ide_jetbrains
```

## Authors

[AUTHORS](./AUTHORS)
