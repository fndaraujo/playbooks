# DIGITAL AUDIO WORKSTATION PACKAGES

Install digital audio workstation packages.

## Requirements

- [community.general.flatpak](https://docs.ansible.com/ansible/latest/collections/community/general/flatpak_module.html)

## Variables

| name | type | default value | description |
| :--- | :---: | ---: | :--- |
| packages_daw_flatpak_packages_daw | List\<string\> | See file | List of packages |
| packages_daw_flatpak_packages_synth | List\<string\> | See file | List of packages |
| packages_daw_flatpak_packages_drummer | List\<string\> | See file | List of packages |
| packages_daw_flatpak_packages_score | List\<string\> | See file | List of packages |

## Dependencies

This role has no dependencies.

## Tags

This role has no tags.

## Example

```yaml
- name: "Setup workstation hosts"
  hosts: "workstations"
  become: "false"
  roles:
    - packages_daw
```

## Authors

[AUTHORS](./AUTHORS)
