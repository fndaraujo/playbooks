# PACKER MACHINE IMAGE BUILDER

Install Packer command line tool.

## Requirements

- [ansible.builtin.copy](https://docs.ansible.com/ansible/latest/collections/ansible/builtin/copy_module.html)
- [ansible.builtin.dnf](https://docs.ansible.com/ansible/latest/collections/ansible/builtin/dnf_module.html)

## Variables

| name | type | default value | description |
| :--- | :---: | ---: | :--- |
| packages_packer_hashicorp_fedora_repository_name | string | See file | Packer repository name |
| packages_packer_hashicorp_fedora_repository_uri | string | See file | Hashicorp repository url for fedora distribution |
| packages_packer_yum_repositories_outers | string | yum.repos.d | Yum third party repositories directory |
| packages_packer_packages_rpm | List\<string\> | See file | Packer rpm packages |

## Dependencies

This role has dependency on either a Fedora or a Fedora variant distribution.

## Tags

This role has no tags.

## Example

```yaml
- name: "Install Packer"
  hosts: "workstations"
  become: "true"
  roles:
    - packages_packer
```

## Authors

[AUTHORS](./AUTHORS)
