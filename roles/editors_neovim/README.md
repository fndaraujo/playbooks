# neovim text editor

Add Neovim text editor.

## Requirements

- [ansible.builtin.dnf](https://docs.ansible.com/ansible/latest/collections/ansible/builtin/dnf_module.html)

## Variables

| name | type | default value | description |
| :--- | :---: | ---: | :--- |
| editors_neovim_packages | List\<string\> | See file | List of packages |

## Dependencies

This role has dependency either on Fedora or a Fedora variant distribution.

## Tags

This role has no tags.

## Example

```yaml
- name: "Install packages"
  hosts: "workstations"
  become: "true"
  roles:
    - editors_neovim
```

## Authors

[AUTHORS](./AUTHORS)
