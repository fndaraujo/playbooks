# FONTS IOSEVKA

Add monospaced font Iosevka.

## Requirements

- [ansible.builtin.file](https://docs.ansible.com/ansible/latest/collections/ansible/builtin/file_module.html)
- [ansible.builtin.unarchive](https://docs.ansible.com/ansible/latest/collections/ansible/builtin/unarchive_module.html)
- [ansible.builtin.get_url](https://docs.ansible.com/ansible/latest/collections/ansible/builtin/get_url_module.html)
- [ansible.builtin.command](https://docs.ansible.com/ansible/latest/collections/ansible/builtin/command_module.html)

## Variables

| name | type | default value | description |
| :--- | :---: | ---: | :--- |
| fonts_iosevka_fonts_directory | string | /home/username/.local/share/fonts | User home default font directory |
| fonts_iosevka_base_directory | string | /home/username/.local/share/fonts/Iosevka | User home default Iosevka font directory |
| fonts_iosevka_download_directory | string | /home/username/Downloads | User home default download directory |

## Dependencies

This role has no dependency.

## Tags

This role has no tags.

## Example

```yaml
- name: "Install fonts"
  hosts: "workstations"
  become: "false"
  roles:
    - fonts_iosevka
```

## Authors

[AUTHORS](./AUTHORS)
