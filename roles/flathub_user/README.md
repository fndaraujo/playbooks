# FLATHUB REMOTE

Add flathub repository with user access.

## Requirements

- [community.general.flatpak_remote](https://docs.ansible.com/ansible/latest/collections/community/general/flatpak_remote_module.html)

## Variables

This role has no variables.

## Dependencies

This role has no dependencies.

## Tags

This role has no tags.

## Example

```yaml
- name: "Setup workstation"
  hosts: "workstations"
  become: "false"
  roles:
    - flathub_user
```

## Authors

[AUTHORS](./AUTHORS)
