# BORG PACKAGE

Add packages for a borg backup environment.

## Requirements

- [ansible.builtin.package](https://docs.ansible.com/ansible/latest/collections/ansible/builtin/package_module.html)

## Variables

| name | type | default value | description |
| :--- | :---: | ---: | :--- |
| borgbackup | List\<string\> | See file | List of packages |

## Dependencies

This role has dependency either on Fedora, or a Fedora variant, and Alpine Linux
distributions.

## Tags

This role has no tags.

## Example

```yaml
- name: "Install packages"
  hosts: "workstations"
  become: "true"
  roles:
    - packages_borg
```

## Authors

[AUTHORS](./AUTHORS)
