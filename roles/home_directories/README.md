# HOME COMMON DIRECTORIES

Create a set of directories in the home directory of an user.

## Requirements

- [ansible.builtin.file](https://docs.ansible.com/ansible/latest/collections/ansible/builtin/file_module.html)

## Variables

| name | type | default value | description |
| :--- | :---: | ---: | :--- |
| home_directories_common | list\<string\> | See vars file | List of common directories in the home of a user |
| home_directories_extended | list\<string\> | See vars file | List of extended directories in the home of a user |

## Dependencies

This role has no dependencies.

## Tags

This role has no tags.

## Example

```yaml
- name: "Setup workstation hosts"
  hosts: "workstations"
  become: "false"
  roles:
    - home_directories
```

## Authors

[AUTHORS](./AUTHORS)
