# UTILITIES PACKAGES

Install utilities packages.

## Requirements

- [community.general.flatpak](https://docs.ansible.com/ansible/latest/collections/community/general/flatpak_module.html)

## Variables

| name | type | default value | description |
| :--- | :---: | ---: | :--- |
| packages_utility_flatpak_packages | List\<string\> | See file | List of packages |

## Dependencies

This role has no dependencies.

## Tags

This role has no tags.

## Example

```yaml
- name: "Setup workstation hosts"
  hosts: "workstations"
  become: "false"
  roles:
    - packages_utility
```

## Authors

[AUTHORS](./AUTHORS)
