# MINIMAL COMMAND LINE INTERFACE ENVIRONMENT

Add a minimal command line interface environment.

## Requirements

- [ansible.builtin.dnf](https://docs.ansible.com/ansible/latest/collections/ansible/builtin/dnf_module.html)

## Variables

| name | type | default value | description |
| :--- | :---: | ---: | :--- |
| cli_minimal_packages | List\<string\> | See file | List of packages |

## Dependencies

This role has dependency either on Fedora or a Fedora variant distribution.

## Tags

This role has no tags.

## Example

```yaml
- name: "Install packages"
  hosts: "workstations"
  become: "true"
  roles:
    - cli_minimal
```

## Authors

[AUTHORS](./AUTHORS)
