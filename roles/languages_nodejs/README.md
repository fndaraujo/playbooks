# NODEJS

Install NodeJS runtime environment.

## Requirements

- [ansible.builtin.dnf](https://docs.ansible.com/ansible/latest/collections/ansible/builtin/dnf_module.html)

## Variables

| name | type | default value | description |
| :--- | :---: | ---: | :--- |
| nodejs_packages_rpm | List\<string\> | See file | List of packages in rpm format |

## Dependencies

This role has dependency either on Fedora or a Fedora variant distribution.

## Tags

This role has no tags.

## Example

```yaml
- name: "Install packages"
  hosts: "workstations"
  become: "true"
  roles:
    - languages_nodejs
```

## Authors

[AUTHORS](./AUTHORS)
