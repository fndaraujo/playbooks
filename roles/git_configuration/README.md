# GIT CONFIGURATION

Create configuration for git.

## Requirements

- [ansible.builtin.file](https://docs.ansible.com/ansible/latest/collections/ansible/builtin/file_module.html)
- [ansible.builtin.copy](https://docs.ansible.com/ansible/latest/collections/ansible/builtin/copy_module.html)
- [community.general.git_config](https://docs.ansible.com/ansible/latest/collections/community/general/git_config_module.html)

## Variables

| name | type | default value | description |
| :--- | :---: | ---: | :--- |
| git_config_xdg_config_home | string | "/home/sysadmin/.config/git" | Location of the git configuration directory |
| git_config_config_file | string | "/home/sysadmin/.config/git/config" | Location of the git configuration file |
| git_config_global_commit_template_file | string | "/home/sysadmin/.config/git/default_commit" | Location of the git commit template file |
| git_config_global_core_excludes_file | string | "/home/sysadmin/.config/git/default_gitignore" | Location of the global gitignore file |

## Dependencies

This role has no dependencies.

## Tags

This role has no tags.

## Example

```yaml
- name: "Setup workstation hosts"
  hosts: "workstations"
  become: "false"
  roles:
    - git_configuration
```

## Authors

[AUTHORS](./AUTHORS)
