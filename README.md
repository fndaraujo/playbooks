# INFRASTRUCTURE PLAYBOOKS

Infrastructure configuration using Ansible.

## Technology

- [Ansible 2.17.0](https://www.ansible.com/)

## Usage

Usage instructions consider a [Fedora Silverblue](https://fedoraproject.org/atomic-desktops/silverblue/)
installation.

Ansible installation is provided through a python virtual environment following
their [documentation](https://docs.ansible.com/ansible/latest/installation_guide/intro_installation.html#installing-and-upgrading-ansible-with-pip).

### Python virtual environment

Create a python virtual environment.

```sh
python3 -m venv .venv
```

Activate the virtual environment.

```sh
source .venv/bin/activate
```

Upgrade pip installation in the virtual environment.

```sh
python3 -m pip install --upgrade pip
```

Install packages in the virtual environment.

```sh
python3 -m pip install -r requirements.txt
```

Verify Ansible installation.

```sh
ansible --version
```

Verify Ansible Community installation.

```sh
ansible-community --version
```

Deactivate the virtual environment.

```sh
deactivate
```

### Ansible vault password file

An Ansible Vault password file is required. Example with the pass tool.

```sh
pass ANSIBLE/vault_password > .vault_password
```

### Inventory configuration

List the inventory.

```sh
ansible-inventory --list
```

Verify host connections.

```sh
ansible all -m ping
```

### Setup workstation

Verify playbook.

```sh
ansible-playbook playbooks/jupiter.yml --check
```

Apply configuration.

```sh
ansible-playbook playbooks/jupiter.yml
```

### Setup toolbx

Verify playbook.

```sh
ansible-playbook playbooks/europa.yml --ask-become-pass --check
```

Apply configuration.

```sh
ansible-playbook playbooks/europa.yml --ask-become-pass
```

## License

[GPL-3.0-only](./LICENSE)

## Authors

[AUTHORS](./AUTHORS)
